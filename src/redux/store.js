import { createStore, applyMiddleware, combineReducers } from "redux";
import todoReducer from "./Reducers/todoReducer";

const thunkMiddleware = require("redux-thunk").default;
const mainReducer = combineReducers({
  todo: todoReducer,
});
const store = createStore(mainReducer, applyMiddleware(thunkMiddleware));
export default store;
