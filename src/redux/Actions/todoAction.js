import Api from "../../ApiManager/index";
import * as geolib from "geolib";
// import _ from "lodash";

export const GetTodos = () => {
  return async function (dispatch) {
    Api.get(`/api/todos`)
      .then(async (response) => {
        if (response.data.success) {
          dispatch({
            type: "SAVE_TODOS",
            payload: response.data,
          });
        }
      })
      .catch((err) => {});
  };
};

export const CreateTodos = () => {
  return async function (dispatch) {
    Api.post(`/api/todos`, {
      params: {
        title,
        description,
      },
    })
      .then(async (response) => {
        if (response.data.success) {
          dispatch({
            type: "CREATE_TODOS",
            payload: response.data,
          });
        }
      })
      .catch((err) => {});
  };
};

export const DeleteTodos = (id) => {
  return async function (dispatch) {
    Api.get(`/api/todos/${id}`)
      .then(async (response) => {
        if (response.data.success) {
          dispatch({
            type: "DELETE_TODOS",
            payload: response.data,
          });
        }
      })
      .catch((err) => {});
  };
};

export const UpdateTodos = (id) => {
  return async function (dispatch) {
    Api.get(`/api/todos/${id}`)
      .then(async (response) => {
        if (response.data.success) {
          dispatch({
            type: "UPDATE_TODOS",
            payload: response.data,
          });
        }
      })
      .catch((err) => {});
  };
};
