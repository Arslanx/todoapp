export const SAVE_TODOS = "SAVE_TODOS";
export const CREATE_TODOS = "CREATE_TODOS";
export const UPDATE_TODOS = "UPDATE_TODOS";
export const DELETE_TODOS = "DELETE_TODOS";
