import { SAVE_TODOS } from "../Actions/todoType";
const initialState = {
  todoList: [] || "",
};

const todoReducer = (state = initialState, action) => {
  console.log(state, "in reducer");
  console.log(action);
  switch (action.type) {
    case SAVE_TODOS:
      return {
        ...state,
        todoList: action.payload,
      };
    default:
      return state;
  }
};
export default todoReducer;
