import React, { Component, Fragment } from "react";
import axios from "axios";
import { connect } from "react-redux";
class TodoList extends Component {
  state = {
    data: [],
  };

  componentDidMount() {
    axios.get("http://localhost:3000/api/todos").then((res) => {
      this.setState({ data: res.data });
    });
  }

  render() {
    return (
      <div>
        <div className="float-left w-100 paddingx_20">
          <div className="container">
            <div className="float-left w-100 header_todo">
              <div className="float-left line_height_2 font_weight_500">
                TODO App
              </div>
              <div className="float-right">
                <button className="btn-light">ToDo List</button>
              </div>
            </div>
            <div className="float-left w-100 border_bottom paddingx_20">
              <div className="container">
                <div className="float-left w-100">
                  <form className="float-left w-100">
                    <div className="heading float-left w-100">Note Details</div>
                    <div className="float-left w-100 form_details">
                      <label className="float-left w-100">Title</label>
                      <input
                        type="text"
                        name="title"
                        placeholder="Enter Title"
                      />
                      <label className="float-left w-100">Text</label>
                      <input
                        type="text"
                        name="title"
                        placeholder="Enter Text"
                      />
                      <button className="submit">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div style={{ justifyContent: "space-evenly" }}>
              {this.state.data.map((todo, index) => (
                <div
                  key={index}
                  className="todo_card float-left"
                  style={{ width: "18rem" }}
                >
                  <div className="todo_card_body float-left w-100">
                    <div className="heading_card float-left w-100">
                      {todo.title}
                    </div>
                    <p className="todo_card_text float-left w-100">
                      {todo.description}
                    </p>
                    <div className="float-left w-100 display_flex px_20">
                      <button className="btn-info mr_15">Edit</button>
                      <button className="btn-danger">Delete</button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state, "states are logged here");
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
