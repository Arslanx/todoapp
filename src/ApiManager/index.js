import axios from "axios";
import { API_URL } from "./client-config";
export default axios.create({
  baseURL: "http://localhost:3000",
});
